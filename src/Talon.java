import java.util.ArrayList;

/**
 * Talon herite de PiledeCartes et implemente PileAbstraite
 * Classe qui opere sur le talon de cartes du jeu FortyEight
 * methodes pour empiler, depiler et afficher
 */

public class Talon extends PiledeCartes implements PileAbstraite {

	
        /**
	 * empiler:
	 * Empile une carte :
	 * 	si le talon contient bien 72 cartes et moins
	 * 
	 * @param  c la carte a empiler.
	 * 
	 * @return un booleen pour savoir si on a effectuer l'operation ou non
	 */
	public boolean empiler(Carte c) {
		if (this.size() < 72)
		{
			this.add(c);
			return true;
		}  
		else 
			return false;
	}
        
        /**
	 * depiler
	 * supprime une carte du talon et la renvoi
	 * 
	 * @return la carte depilee
	 */ 
	public Carte depiler() { 
		Carte c=this.get(this.size()-1);
		this.remove(this.size()-1);
		return c;	
	}
        
        /**
	 * afficher
	 * Affiche le talon 
	 * _ si il est vide
	 * X ,c'est a dire carte cachee sinon
	 */
	public void afficher() {
		if(this.isEmpty())
			System.out.println("_");
		else
			System.out.println("X");
	}
}
