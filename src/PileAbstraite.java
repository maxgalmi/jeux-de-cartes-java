/**
 * 
 * Au depart c'etait une pile abstraite , mais on a du la transformer en interface au cours du projet
 * 
 */
public interface PileAbstraite {

	public abstract boolean empiler(Carte deck);

	public abstract Carte depiler();

	public abstract void afficher();

}
