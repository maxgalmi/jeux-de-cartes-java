import java.util.ArrayList;


/**
 * PileBases herite de PiledeCartes et implemente PileAbstraite
 * Classe qui gere les op�rations sur la pile de bases de FortyEight
 * Contient une methode pour empiler , pour depiler et afficher
 */

public class PileBases extends PiledeCartes implements PileAbstraite {

	
/**
 * empiler:
 * Empile une carte :
 * 	Si la pile est vide : uniquement les as
 *  Si la pile n'est pas vide : uniquement les cases de meme couleur et de consecutive
 * 
 * @param  c la carte a empiler.
 * 
 * @return un booleen pour savoir si on a effectuer l'operation ou non
 */
	public boolean empiler(Carte c) {
		if (this.isEmpty())
		{
			if (c.valeur==1)
			{
				this.add(c);
				return true;
			}
			else 
				return false;
		}

		else if 
		(this.get(this.size()-1).couleur==c.couleur && this.get(this.size()-1).valeur+1==c.valeur)
		{
			this.add(c);
			return true;
		}
		else 
			return false;
	}
        /**
	 * depiler
	 * ne renvoie rien , depiler une carte d'une base n'est pas autorise
	 */
	public Carte depiler() {
		return null;
	}
        
        /**
	 * afficher
	 * Affiche les bases
	 */
	public void afficher() {
		if (this.isEmpty())
			System.out.print("X ");
		
                else{
                    if(this.get(this.size()-1).valeur<11)
                        
		    System.out.print(this.get(this.size()-1).valeur);
                     else if (this.get(this.size()-1).valeur==11)
                    System.out.print("V"); 
                
                    else if (this.get(this.size()-1).valeur==12)
                    System.out.print("D"); 
                
                    else 
                    System.out.print("R");
                    System.out.print(this.get(this.size()-1).couleur);
                }    
	}
}
