import java.util.ArrayList;

/**
 * Pot herite de PiledeCartes et implemente PileAbstraite
 * Classe qui opere sur le pot de cartes du jeu FortyEight
 * methodes pour empiler, depiler et afficher
 */
public class Pot extends PiledeCartes implements PileAbstraite {

	

	public boolean empiler(Carte c) { 
		if (this.size() < 72)
		{
			this.add(c);
			return true;
		}  
		else 
			return false;
	}
        
        /**
	 * depiler
	 * supprime une carte du pot et la renvoi
	 * 
	 * @return la carte depilee
	 */ 

	public Carte depiler() {
		Carte c=this.get(this.size()-1);
		this.remove(this.size()-1);
		return c;	
	}
        /**
	 * afficher
	 * Affiche le pot
	 * _ si il est vide
	 * les cartes dessus sinon
	 */ 
	public void afficher(){
		if(this.isEmpty())
			System.out.println("_");
                else{
                    if(this.get(this.size()-1).valeur<11)
                        
		    System.out.print(this.get(this.size()-1).valeur);
                     else if (this.get(this.size()-1).valeur==11)
                    System.out.print("V"); 
                
                    else if (this.get(this.size()-1).valeur==12)
                    System.out.print("D"); 
                
                    else 
                    System.out.print("R");
                    System.out.print(this.get(this.size()-1).couleur);
                }
        }


}