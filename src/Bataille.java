
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Bataille implements Runnable, Jeu{
    /**
     * Pile de Cartes du joueur
     */
    PiledeCartes p1 = new PiledeCartes();
    /**
     * Pile de cartes de l'ordinateur
     */
    PiledeCartes p2 = new PiledeCartes();
    /**
     * Cartes actuellement tirés par le joueur
     */
    PiledeCartes cartestires1 = new PiledeCartes();
    /**
     * Cartes actuellement tirés par l'ordinateur
     */
    PiledeCartes cartestires2 = new PiledeCartes();
   
    /**
     * Constructeur du jeu, crée un paquet de 52 cartes, les mélanges, puis les distribuent aux 2 joueurs
     */
    public Bataille(){
         PiledeCartes P = new PiledeCartes(); //Création d'un paquet de 52 cartes;
        for(int i = 1;i<14;i++){
            P.add(new Carte(i,"Ca"));
            P.add(new Carte(i,"Co"));
            P.add(new Carte(i,"P"));
            P.add(new Carte(i,"T"));
            
        }
        P.melangerCartes();
        for(int i=0; (i< P.size()/2); i++)
            p1.add(P.get(i));
            
        for(int i=P.size()/2; i< P.size(); i++)
            p2.add(P.get(i));    
    }
    
    /**
     * Affiche le plateau de jeu
     */
    public void afficher(){
        System.out.print("Ordinateur [" + p2.size() + "]" );
        cartestires2.afficherPile();
        System.out.print("Joueur     [" + p1.size() + "]" );
        cartestires1.afficherPile();
    }
    
   
    
    
    /**
     * Détermine le gagnant de la manche
     * @return le joueur gagnant 0: match nul 1:joueur, 2:ordinateur 
     */
    public int gagnant(){
        if (!cartestires1.isEmpty()&&!cartestires2.isEmpty()){
            if (cartestires1.get(cartestires1.size()-1).valeur>cartestires2.get(cartestires2.size()-1).valeur){
                return 1;
            }else if(cartestires1.get(cartestires1.size()-1).valeur<cartestires2.get(cartestires2.size()-1).valeur){
                return 2;
            }else return 0;
            
        }else return 0;
    }
    
    /**
     * Ajoute les cartes tirés par les 2 joueurs à la pile du gagnant
     */
    public void ajoutergagnant(int n){
        if(n==1){
            while (!cartestires1.isEmpty())
                p1.add(cartestires1.remove(0));
            while (!cartestires2.isEmpty())
                p1.add(cartestires2.remove(0));
        }else{
            while (!cartestires1.isEmpty())
                p2.add(cartestires1.remove(0));
            while (!cartestires2.isEmpty())
                p2.add(cartestires2.remove(0));
            
        }
        
    }
    
    /**
     * Détermine si l'un des 2 joueurs n'a plus de cartes
     * @return true si l'un des 2 joueurs n'a plus de cartes, false sinon
     */
    public boolean fin(){
        if (p1.isEmpty()||p2.isEmpty())
            return true;
        else return false;
    }
    
    /**
     * Jouer au jeu
     */
    public void jouer(){
        Scanner sc = new Scanner(System.in);
        int gagnant =0;
        int deroulement = 0; //1 si manuel, 2 si automatique, 3 si instantané
        while (deroulement<1||deroulement>3){
            System.out.println("Déroulement de la partie : 1 : manuel, 2 : automatique, 3 : instantané");
            deroulement=sc.nextInt();
        }
            
        while(!fin()){
            if (deroulement==1){
            System.out.println("Appuyer sur une touche pour tirer une carte");
            sc.next();
            }else if (deroulement==2){
            Thread t1 = new Thread(new Bataille());
            t1.start();
            try {
                t1.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Bataille.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            cartestires1.add(p1.remove(0));
            cartestires2.add(p2.remove(0));
            afficher();
            if(gagnant()==1){
               
             ajoutergagnant(1);   
             System.out.println("Le joueur gagne la manche"); 
            }else if(gagnant()==2){
              ajoutergagnant(2);  
              System.out.println("L'ordinateur gagne la manche");
            }else{ // cas d'égalité : bataille
                gagnant =0;
                while (gagnant ==0){
                if (p1.size()<2||p2.size()<2){
                    System.out.append("Pas assez de cartes pour une bataille. ");//si un des joueurs n'as pas assez de cartes pour la bataille : fin du jeu
                    if (p1.size()==p2.size()){
                      System.out.println("Match nul"); return;
                    }else if (p2.size()>p1.size()){
                        System.out.println("L'ordinateur gagne"); return;
                    }else System.out.println("Le joueur gagne"); return;
                }else{  
                    System.out.println("Bataille");
                   if (deroulement==1){ 
                  System.out.println("Appuyer sur une touche pour tirer une carte");
                     sc.next();}
                   else if (deroulement==2){
                    Thread t2 = new Thread(new Bataille());
                     t2.start();
                     try {
                       t2.join();
                    } catch (InterruptedException ex) {
                    Logger.getLogger(Bataille.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   }
                    p1.get(0).statut = "caché";
                    p2.get(0).statut = "caché";
                    cartestires1.add(p1.remove(0));
                    cartestires1.add(p1.remove(0));
                    cartestires2.add(p2.remove(0));
                    cartestires2.add(p2.remove(0));
                    afficher();
                    gagnant = gagnant();
                    if(gagnant==1){
                    ajoutergagnant(1);
                    System.out.println("Le joueur gagne la manche"); 
                    }else if(gagnant==2){
                    ajoutergagnant(2);   
                    System.out.println("L'ordinateur gagne la manche");
                    }
                }  
                }p1.setVisible();p2.setVisible();}p1.melangerCartes();p2.melangerCartes();
        } 
                    if (p1.size()==p2.size()){
                      System.out.println("Match null"); 
                    }else if (p2.size()>p1.size()){
                        System.out.println("L'ordinateur gagne");
                    }else System.out.println("Le joueur gagne"); 
        
    }
    
     public static void main(String[] args) {
        Bataille b = new Bataille();
        b.afficher();
        b.jouer();
    }

    /**
     * Marque une pause d'une seconde
     */
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Bataille.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
}
