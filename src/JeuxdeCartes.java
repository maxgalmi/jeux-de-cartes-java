import java.util.Scanner;

/*
 * Classe principale du projet
 * 
 */
public class JeuxdeCartes {
	public static void main(String[] args) {
		int n = 0;
                Scanner sc =new Scanner(System.in);
                while(n<1||n>3){
                System.out.println("Choisir jeu 1:Simple Simon, 2:Forty Eight 3:Bataille");
                n=sc.nextInt();
                }
                if (n==1){
                    Jeu sp = new SimpleSimon();
                    sp.afficher();
                    sp.jouer();
                }else if (n==2){
                    Jeu F=new FortyEight();
                    F.jouer();                     
                }else{
                    Jeu b = new Bataille();
                    b.afficher();
                    b.jouer();
                }
                    
                    
        }          
}