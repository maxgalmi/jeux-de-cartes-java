
import java.util.Scanner;
import java.util.Stack;

/**
 * Jeu Simple Simon
 */
public class SimpleSimon implements Jeu{
    /**
     * Tableau contenant les piles du jeu
     */
    PiledeCartes[] tab = new PiledeCartes[10];
    /**
     * Pile de tableau de pile, permet de stocker la configuration des piles
     */
    Stack<PiledeCartes[]> p = new Stack<PiledeCartes[]>();
    
    
   /**
    * Constructeur du jeu simple simon
    * <p> On crée un paquet de 52 cartes, puis on les distribue sur les piles
    * @see SimpleSimon#tab
    */    
    public SimpleSimon(){
        PiledeCartes P = new PiledeCartes(); //Création d'un paquet de 52 cartes;
        for(int i = 1;i<14;i++){
            P.add(new Carte(i,"Ca"));
            P.add(new Carte(i,"Co"));
            P.add(new Carte(i,"P"));
            P.add(new Carte(i,"T"));
            
        }
        P.melangerCartes();
        for (int i=0;i<10;i++)
            tab[i] = new PiledeCartes();
        //Création des piles de cartes pour le jeu
        for (int i=0;i<8;i++)
            tab[0].add(P.get(i));
        for (int i=8;i<16;i++)
            tab[1].add(P.get(i));
        for (int i=16;i<24;i++)
            tab[2].add(P.get(i));
        for (int i=24;i<31;i++)
            tab[3].add(P.get(i));
        for (int i=31;i<37;i++)
            tab[4].add(P.get(i));
        for (int i=37;i<42;i++)
            tab[5].add(P.get(i));
        for (int i=42;i<46;i++)
            tab[6].add(P.get(i));
        for (int i=46;i<49;i++)
            tab[7].add(P.get(i));
        for (int i=49;i<51;i++)
            tab[8].add(P.get(i));
            tab[9].add(P.get(51));
            
        
        
        
    }
    /**
     * Fait une copie de la configuration des piles du jeu actuelles
     * @return la copie du tableau
     */
    public PiledeCartes[] copiertableau(){ 
        PiledeCartes[] tab2 = new PiledeCartes[10];
            for (int i=0;i<10;i++){
                tab2[i] = new PiledeCartes();
                for (int j=0; j<tab[i].size();j++)
                    tab2[i].add(tab[i].get(j));
            }
       return tab2;
}
    /**
     * Annule le dernier mouvement
     */
    public void retour(){ 
        if (!p.isEmpty())
          tab =  p.pop();
        if (!p.isEmpty())
          tab =  p.pop(); afficher();
    }
    /**
     * Affiche le plateau de jeu
     */
    public void afficher(){
        
        for (int i=0;i<10;i++){
            System.out.print("[" + (i+1) + "] "); tab[i].afficherPile();
        }
       
    }
    /**
     * Determine si toutes les piles sont soit vides, soit des sequences monocolores completes(fin du jeu)
     */
    public boolean fin(){ 
        for (int i=0;i<10;i++){
            if (!tab[i].estunesequencemonocolore(1)||(tab[i].size()!=0&&tab[i].size()!=13))
                return false;
        }System.out.println("Vous avez gagné");
        return true;
    }
    
   
    /**
     * Jouer
     */
    public void jouer(){
      
      Scanner sc = new Scanner(System.in) ; 
      int n=-1;
      int m =-1;
      int l = -1;
      boolean annule = false;
      while (!fin()) {
           p.push(copiertableau()); //copie le plateau de jeu actuel dans la pile
          while (n<0||n>10||!(tab[n-1].size()>0)){
          System.out.println("Pile contenant la séquence à déplacer? (0 : annuler dernier déplacement)");
            n = sc.nextInt();
            if (n==0){
                annule = true;
                retour();
            break;}
            else if (!(tab[n-1].size()>0))
                System.out.println("Pile vide, en choisir une autre1");
          }
          if (!annule){
          while (m<0||m>tab[n-1].size()||!tab[n-1].estunesequence(m)){
               System.out.println("Position de la carte à déplacer  (0 : annuler déplacement)");
                 m= sc.nextInt();
                 if (m==0){
                annule = true;break;}
                 else if (!tab[n-1].estunesequence(m))
                System.out.println("Pas un séquence.Déplacement impossible");
          }  }     
          
          if (!annule){
          while (l<0||l>10||l==n||!tab[n-1].estdeplacable(tab[l-1], m)){
          System.out.println("Ou déplacer? (0 : annuler déplacement)");
            l = sc.nextInt();
            if (l==0){
                annule = true; break;}
            
            if (l<1||l>10||l==n)
                System.out.println("Choisir une autre pile");
            if (!tab[n-1].estdeplacable(tab[l-1], m))
                System.out.println("deplacement impossible ici");
            
            
          }}  
            
          
          if (!annule){
          tab[n-1].deplacersequence(tab[l-1], m);        afficher();} n=-1;m=-1;l=-1; annule = false;
      } 
    }
    
    
     public static void main(String[] args) {
        SimpleSimon sp = new SimpleSimon();
        sp.afficher();
        sp.jouer();
    }
    
    
}
