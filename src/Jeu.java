/**
 * Interface jeu
 */
public interface Jeu {
    /**
     * affiche le plateau de jeu
     */
    public void afficher();
    /**
     * Jouer au jeu
     */
    public void jouer();
    /**
     * Determine la fin du jeu
     * @return true si l'une des condtitions d'arret du jeu st rempli, false sinon
     */
    public boolean fin();
}
