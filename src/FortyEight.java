import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/**
 * 
 * Jeu FortyEight
 */

public class FortyEight implements Jeu {

	ArrayList<Carte> Deck; 
	PilesFour[] plat;
	Talon T;
	Pot P;
	PileBases[] gare;
        

	public FortyEight() {
		Deck=new ArrayList<Carte>();
		plat=new PilesFour[8];
		T=new Talon();
		P=new Pot();
		gare=new PileBases[8];
		creerJeu();
		genererHuit();
		genererTalon();
		genererBases();
		afficher(); 

	}

	public void creerJeu() {
		for (int j=0 ; j<2 ;j++ ){
			for(int i =1 ;i<14; i++){
				Deck.add(new Carte(i,"Ca"));
				Deck.add(new Carte(i,"Co"));
				Deck.add(new Carte(i,"Pi"));
				Deck.add(new Carte(i,"Tr"));
			}
		}
		Collections.shuffle(Deck); 
	}

	public void genererHuit() { 
		for (int i=0;i<8;i++){ 
			plat[i] = new PilesFour();
			for (int j=0; j<4 ;j++){
				plat[i].empiler(Deck.get(Deck.size()-1));
				Deck.remove(Deck.get(Deck.size()-1));
			}
		}
	}

	public void genererTalon() { 
		for (int i=0; i< 72; i++){
			T.empiler(Deck.get(i));
		}
	}

	public void reGenererTalon() {
		Collections.reverse(P);
		for (int i=0; i<P.size(); i++){
			T.empiler(P.get(i));
		}
		T.add(0,(((P.get(0)))));
		P.clear();
	}

	public void genererBases() {
		for (int i=0; i<8; i++){ 
			gare[i] = new PileBases();
		}
	}

	public void piocherTalon() {
		Carte c=T.depiler();
		P.empiler(c);
	}

	public void deplacementVersBase() {
		Carte c=P.depiler();
		carteToBase(c);
		afficher();
	}

	public boolean carteToBase(Carte c){
		for (int i=0; i<8 ; i++){ 
			if(gare[i].empiler(c)){
				return true;
			}
		}
		return false;
	}


	public boolean carteToPlateau(int p) {
		Carte c=P.depiler();
		if (plat[p-1].get(plat[p-1].size()-1).couleur==c.couleur && plat[p-1].get(plat[p-1].size()-1).valeur==c.valeur+1)
		{ 
			plat[p-1].empiler(c);
			afficher();
			return true;
		}
		else 
			P.empiler(c);
		return false;
	}


	public boolean plateauToPlateau(int m, int n) {
		Carte c=plat[m-1].depiler();
		if (plat[n-1].get(plat[n-1].size()-1).couleur==c.couleur && plat[n-1].get(plat[n-1].size()-1).valeur==c.valeur+1)
		{
			plat[n-1].empiler(c);
			afficher();
			return true;
		}
		else 
			plat[m-1].empiler(c);
		return false;
	}

	public void plateauToBase(int k) {
		Carte c= plat[k-1].depiler();
		System.out.println("Action sur la pile "+k);
		if(!carteToBase(c)){
			plat[k-1].empiler(c);
		}
		afficher();
	}

	public boolean fin(){
		for(int i=0; i<8; i++){
			if (gare[i].size()!=13)
				return false;
		}
		System.out.println("You win!");
		return true;
	}

	public void afficher(){ 
		System.out.print("[Bases]");

		for (int k=0;k<8;k++)
			gare[k].afficher();
		System.out.println();

		for (int i=0;i<8;i++){
			System.out.print("["+(i+1)+"] ");
			plat[i].afficher();
		}

		System.out.print("[Talon : " + T.size() + ")]\t");			
		T.afficher();	

		System.out.print("[Pot : "+ P.size() + "]\t");	
		P.afficher();	
		System.out.println();
	}
        
        public void jouer(){
            int cmpt=0;

		
		while (!fin())
		{

			if (T.isEmpty())
			{
				if (cmpt<1)
				{
					System.out.println("Last chance \n");	
					reGenererTalon();
					cmpt++;
				}
				else
				{
					System.out.println("You lose");
					System.exit (0);
				}
			}

			else 
			{
				Scanner sc= new Scanner(System.in);
				System.out.println("1/ Tirer une carte du talon  2/ Tirer une carte du Pot 3/ Tirer une carte d'une pile du plateau");
				int i=sc.nextInt();
				if (i==1)
				{
					piocherTalon();
					afficher();
				}
				else if (i==2)
				{int j = 0;
                                   while(j!=1&&j!=2) {
					System.out.println("1/ Vers une base 2/ Vers une pile du plateau");
					j=sc.nextInt();}
					
						if (j==1)
						{
							deplacementVersBase();
						}
						else if(j==2)
						{
							System.out.println("Vers quelle pile?");
							int k=sc.nextInt();
							carteToPlateau(k);
						}
						else 				
							System.out.println("Entre 1 et 2...");

					
				}
				else if(i==3)
				{
					System.out.println("1/ Vers une pile du plateau 2/ Vers une base");
					int l=sc.nextInt();
					if (l==1)
					{
						System.out.println("Selectionnez une pile de depart");
						int m=sc.nextInt();
						System.out.println("Selectionnez une pile d'arrivee");
						int n=sc.nextInt();
						plateauToPlateau(m, n);
					}
					else if (l==2)
					{
						System.out.println("Selectionnez une pile");
						int k=sc.nextInt();
						plateauToBase(k);
					}
				}
				else 
					System.out.println("Entre 1 et 3...");
			}	
		}
        }
        
        
        public static void main(String[] args){
            FortyEight F=new FortyEight();
            F.jouer();
            
        }
        
}
