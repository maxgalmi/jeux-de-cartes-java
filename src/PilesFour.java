import java.util.ArrayList;

public class PilesFour extends PiledeCartes implements PileAbstraite {

	public boolean empiler(Carte deck) {
		if (this.size() < 13)
		{
			this.add(deck);
			return true;
		}  
		else 
			return false;
	}

	public Carte depiler() {
		Carte c=this.get(this.size()-1);
		this.remove(this.size()-1);
		return c;	
	}

	public void afficher() {
		super.afficherPile();
	}

}
