/**
 * Représente une carte
 * 
 */
public class Carte {
    /**
     * Valeur de la carte
     */
    public int valeur;
    /**
     * Couleur de la carte 
     */
    public String couleur; 
    /**
     * Statut de la carte : visible par défaut
     */
    public String statut = "visible"; 
    
    
    public Carte(int valeur, String couleur){
        this.valeur=valeur;
        this.couleur=couleur;
    }
    
}
