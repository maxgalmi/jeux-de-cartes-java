
import java.util.ArrayList;
import java.util.Collections;

/**
 * Pile decartes
 */
public class PiledeCartes extends ArrayList<Carte>{
    


    /**
     * Affiche les cartes de la pile
     */
    public void afficherPile(){
        
            for (int i = 0; i<this.size();i++){
                //On affiche la valeur de la carte
               if (this.get(i).statut.equals("visible")){ 
                if (this.get(i).valeur<11)
                    System.out.print(this.get(i).valeur); 
                   
                else if (this.get(i).valeur==11)
                    System.out.print("V"); 
                
                else if (this.get(i).valeur==12)
                    System.out.print("D"); 
                
                else if (this.get(i).valeur==13)
                    System.out.print("R"); 
                //Puis sa couleur;
                if (i != this.size()-1)
                System.out.print(this.get(i).couleur + "," + " ");
                else
                    System.out.print(this.get(i).couleur);
            }else{
                   if (i != this.size()-1)
                        System.out.print("X" + "," + " ");
                   else{
                    System.out.print("X");}
               }
                   
            } System.out.println();
        }
   
    
   
    /**
     * Mélange les cartes de la pile
     */
    public void melangerCartes(){
        if(this.size()>1){
        for (int i=0;i<1000;i++){
            int n = (int)(Math.random()*this.size());
            int m = (int)(Math.random()*this.size());
            Collections.swap(this, n, m);
        }}
    } 
    
    /**
     * Détermine si la pile est une séquence, à partir de la carte donnée en paramètre jusqu'à la fin de la pile
     * @param n
     *  Position de la carte du début
     * @return true si c'est une séquence, false sinon
     */
    public boolean estunesequence(int n){
       
        for(int i=n-1; i<this.size()-1;i++){
            if(this.get(i).valeur!=(this.get(i+1).valeur +1)) //On regarde sila valeur de la carte actuelle est égale à celle de la carte suivante + 1;
                return false;
        }return true;
        
    }
    
     /**
     * Détermine si la pile est une séquence monocolore, à partir de la carte donnée en paramètre jusqu'à la fin de la pile
     * @param n
     *  Position de la carte du début
     * @return true si c'est une séquence, false sinon
     * @see PiledeCartes#estunesequence(int)
     */
    public boolean estunesequencemonocolore(int n){
        if (estunesequence(n)){
            for(int i=n-1; i<this.size()-1;i++){
            if(!this.get(i).couleur.equals(this.get(i+1).couleur)) //On regarde sila valeur de la carte actuelle est égale à celle de la carte suivante + 1;
                return false;
            }return true;
        
        }else return false;
        
    }
    public boolean estdeplacable(PiledeCartes p, int n){
        if (p.size()>0){
            if (this.get(n-1).valeur==(p.get(p.size()-1).valeur-1))
                return true;
            else return false;
        }else return true;
    }
    
    public void deplacersequence(PiledeCartes p, int n){
        for(int i=n-1; i<this.size();i++){
            p.add(this.get(i));
        }for(int i=this.size()-1; i>n-2;i--){
            this.remove(i);
        }
    }
    /**
     * Met toutes les cartes de la pile en statut visible
     */
    public void setVisible(){
        for (int i = 0; i<this.size();i++){
            this.get(i).statut="visible";
        }
    }
    
   


}